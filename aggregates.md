# Continuous Aggregates

> Eine Art Materialized View mit laufenden Updates und History.

## Überblick

- Downsampling/Aggregierung der Rohdaten
- dadurch performante long-range Queries
- immer aktuell!
- [time_bucket()](https://docs.timescale.com/api/latest/hyperfunctions/time_bucket/) Hyperfunction **muss** verwendet werden

```sql
CREATE MATERIALIZED VIEW on_hour_summary WITH (timescaledb.continuous) AS
  SELECT time_bucket('1 hour', time) one_hour,
     MAX(cpu_usage),
     AVG(cpu_usage), ...
  FROM cpu
WITH NO DATA;  --> Updates: manual, od. Refresh Policy
```

## Background

- basiert auf Hypertable (Partitionen)
- bei Source Change werden Teile ("partials") refreshed
- ermöglicht Data Retention Policies getrennt von Quelle
- **CA immer aktuell**: nicht refreshed/materialized/aggregated Data werden automatisch von der Quelltabelle via **Realtime Aggregates** einbezogen (also zB sehr aktuelle Daten, welche via Refresh Policy noch nicht bearbeitet wurden (= aktueller Bucket); weniger performant)
- für automatisches Materializing muss eine Refresh Policy definiert werden
- [Einschränkungen von CAs:](https://docs.timescale.com/api/latest/continuous-aggregates/create_materialized_view/) nur eine Hypertable; keine JOINs (Ergebnis des CA kann sehr wohl gejoined werden)
- zu `time_bucket()`:
  - TIMESTAMPTZ Buckets sind UTC aligned (=> daily buckets are aligned to midnight UTC)
  - "aktueller" Bucket nie materialized; `time_bucket('1 hour', time)` => Daten bis 1h zurück immer via Realtime Agg. (Rohdaten)
- Chunk Time Interval kann angepasst werden, siehe [`set_chunk_time_interval()`](https://docs.timescale.com/api/latest/hypertable/set_chunk_time_interval/#sample-usage)

## Refresh Policies

- definiert Interval u. Range für CA Refresh
- entscheidet darüber, wie alt Daten sein dürfen, um Refresh zu triggern
- API Docs: [add_continuous_aggregate_policy()](https://docs.timescale.com/api/latest/continuous-aggregates/add_continuous_aggregate_policy/)

```sql
SELECT add_continuous_aggregate_policy(
  continuous_aggregate => 'on_hour_summary',
  start_offset         => '1 day',
  end_offset           => '1 minute',
  schedule_interval    => '1 hour'  --> weil: time_bucket('1 hour', time)
);
```

Bei Start dieser Refresh Policy werden Werte `1 day` in der Vergangenheit berücksichtigt. Der `1 hour` Interval reicht völlig, da die Bucket Width des Continuous Aggregates auch 1 Stunde beträgt. **Grundregeln:**

- `start_offset` = mind. 2 Bucket Widths
- `add_continuous_aggregate_policy(start_offset) < add_retention_policy(drop_after)`

## FAQ

Warum liefert ein Continuous Aggregate mit `WITH NO DATA` Definition trotzdem Daten?

- durch das Realtime Aggregate Feature werden automatisch Werte von Quelltabelle einbezogen
- weniger performant, da (noch) nichts "materialized"

Ich möchte stündliche Time Buckets, die Rohdaten sollen jedoch nach 10 Minuten gelöscht werden um Platz zu sparen. Geht das?

- geht nicht, da immer ein voller Bucket aggregiert werden muss
- dies wäre hier nicht möglich, da nur 10min an Daten vorhanden sind, wir aber mind. eine volle Stunde brauchen

Warum kann ein Continuous Aggregate nicht auf einer anderen Continuous Aggregate erzeugt werden?

- (noch) nicht möglich aufgrund Design Entscheidungen im Bezug auf Multinode Setup

## Verschiedenes

- schnelles `count(*)` mit [approximate_row_count()](https://docs.timescale.com/api/latest/hyperfunctions/approximate_row_count/)

```sql
SELECT * FROM approximate_row_count('conditions');

-- data range
select min(time), max(time) from nft_sales;

-- utc timestamp
select now() at time zone 'utc';
```

## Admin

```sql
-- realtime aggregates deaktivieren
alter materialized view cpu_hourly set (timescaledb.materialized_only = true);

-- alle CAs
-- materialization_hypertable_name --> _materialized_hypertable_10
select * from timescaledb_information.continuous_aggregates;
-- CA size
select pg_size_pretty(total_bytes)
from hypertable_detailed_size('_timescaledb_internal._materialized_hypertable_10');
```

## Resources

- Talk: [Common questions with Continuous Aggregates](https://www.youtube.com/watch?v=btckDZnsBIg)
- Docs: [Core Concepts](https://docs.timescale.com/timescaledb/latest/overview/core-concepts/continuous-aggregates/), [API Reference](https://docs.timescale.com/api/latest/continuous-aggregates/)
