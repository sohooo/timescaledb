# TimescaleDB

- Datenmigration InfluxDB=>TimescaleDB möglich via Outflux: https://docs.timescale.com/timescaledb/latest/how-to-guides/migrate-data/migrate-influxdb/#outflux-installation
- memory tuning: 64GB,…
- Compression by tag=hostname => schneller bei Filtering nach hostname, da zuerst gefiltert werden kann, u. dann erst dekomprimiert werden muss
- includes a background job scheduling framework for automating data management tasks, such as enabling easy data retention policies.
- indexing best practices: https://docs.timescale.com/timescaledb/latest/how-to-guides/schema-management/indexing/
- [use trigger](https://docs.timescale.com/timescaledb/latest/how-to-guides/schema-management/triggers/) to to write specific `conditions` data to `error_conditions` table
- release compatibility matrix: https://docs.timescale.com/timescaledb/latest/how-to-guides/update-timescaledb/
- `CREATE EXTENSION timescaledb_toolkit;` - more time-series functions (see hyperfunctions)
- API reference: https://docs.timescale.com/api/latest/


## configuration

- `timescaledb-tune` - https://docs.timescale.com/timescaledb/latest/how-to-guides/configuration/timescaledb-tune/
- `pgtune` - https://pgtune.leopard.in.ua with type: data warehouse
- increase `max_locks_per_transaction` - https://docs.timescale.com/timescaledb/latest/how-to-guides/configuration/about-configuration/#transaction-locks

```bash
max_locks_per_transaction = 200    # default: 64
timescaledb.telemetry_level = off  # default: basic
``` 

## hypertables

- best practices: https://docs.timescale.com/timescaledb/latest/how-to-guides/hypertables/best-practices/
- default time interval: 7 days; can be changed with set_chunk_time_interval
- By default, TimescaleDB automatically creates a time index on your data when a hypertable is created.

```sql
-- get chunk sizes:
SELECT * FROM chunks_detailed_size('dist_table')
  ORDER BY chunk_name, node_name;
```

### deleting data
To delete old data, use drop_chunks instead. drop-chunks is more performant because it deletes entire chunks by removing files, rather than deleting individual rows that need vacuuming. To learn more, see the data retention section.

```sql
-- This drops all chunks from the hypertable conditions that only include data older than this duration,
-- and does not delete any individual rows of data in chunks.
SELECT drop_chunks('conditions', INTERVAL '24 hours');
```

- time_bucket() https://docs.timescale.com/timescaledb/latest/how-to-guides/query-data/select/
- advanced analytic queries: https://docs.timescale.com/timescaledb/latest/how-to-guides/query-data/advanced-analytic-queries/


## continuous aggregates

- continuous aggregates for large/slow queries: https://docs.timescale.com/timescaledb/latest/how-to-guides/continuous-aggregates/about-continuous-aggregates/
- refresh policy and compression policy
- if you have a data retention policy that removes all data older than two weeks, the continuous aggregate policy will only have data for the last two weeks.
- compress CAs https://docs.timescale.com/timescaledb/latest/how-to-guides/continuous-aggregates/compression-on-continuous-aggregates/
- retention policies separate to raw hypertable: if you have a hypertable with a retention policy of a week and a continuous aggregate with a retention policy of a month, the raw data is kept for a week, and the continuous aggregate is kept for a month.


## compression

- data is converted to a single row containing an array
- doesnt support DELETE actions

1. alter table, define compression segment
2. add compression policy

```sql
-- compress junks oder than 7 days
SELECT add_compression_policy('example', INTERVAL '7 days');
-- info
SELECT * FROM chunk_compression_stats('example');
SELECT show_chunks('example', older_than => INTERVAL '3 days');
```

## user defined actions

- create automatic periodic tasks: https://docs.timescale.com/timescaledb/latest/how-to-guides/user-defined-actions/


## data retention

- https://docs.timescale.com/timescaledb/latest/how-to-guides/data-retention/

```sql
-- delete chunks older than 24 hours
SELECT add_retention_policy('conditions', INTERVAL '24 hours');

-- remove policy
SELECT remove_retention_policy('conditions');

-- view scheduled jobs
SELECT * FROM timescaledb_information.job_stats;

-- for our aggregate
SELECT add_retention_policy('conditions_summary_daily', INTERVAL '600 days');
```

- this 24h policy is a problem if we have a continuous aggregate for say 7 days; we would update the aggregate when dropping the chunk from the base table, and the aggregate would contain no data anymore
- we could fix this by defining a more suitable interval for the retention policy, like `INTERVAL '30 days'` 

## replication

- https://docs.timescale.com/timescaledb/latest/how-to-guides/replication-and-ha/configure-replication/
- most common streaming replication use case is asynchronous replication with one or more replicas


## hyperfunctions

- for time-series queries: https://docs.timescale.com/timescaledb/latest/how-to-guides/hyperfunctions/
- some are included; lots more in `timescaledb_toolkit` extension: https://docs.timescale.com/timescaledb/latest/how-to-guides/hyperfunctions/install-toolkit/#install-toolkit-on-self-hosted-timescaledb - `CREATE EXTENSION timescaledb_toolkit;`
- docs about various hyperfunction types: https://docs.timescale.com/timescaledb/latest/how-to-guides/hyperfunctions/approx-count-distincts/

### to consider

- use percentile approximation instead of avg: https://www.timescale.com/blog/how-percentile-approximation-works-and-why-its-more-useful-than-averages/
  - If your workflow involves estimating 99th percentiles, then choose `tdigest`
  - If you're more concerned about getting highly accurate median estimates, choose `uddsketch`
- https://docs.timescale.com/timescaledb/latest/tutorials/grafana/visualize-missing-data/
