# Data Retention

TimescaleDB Data Retention Policies sind dazu da, alte Daten effizient zu löschen. `DELETE`s sind bei größeren Datenmengen langsam: alle Tabellenänderungen resultieren in MVCC Aktionen u. `VACUUM`[^1] (sowie Locks). Der Drop eines Table Chunks (Partition) hingegen nicht (od. höchstens sehr kurz).

## Config

- Hypertable `chunk_time_interval` ist per Default `interval '7 days'`; es werden nur komplette Chunks gedroppt, daher besser kleinere Chunks

## Admin

```sql
select add_retention_policy('table', interval '24 hours');
select remove_retention_policy('table');

-- alle Policies
select * from timescaledb_information.job_stats;

-- manuell Chunks löschen
select drop_chunks('table', interval '10 hours');

-- Summary
select
    pg_size_pretty(sum(total_bytes)) as chunk_size,
    count(*) chunks_count
from chunks_detailed_size('table');

-- Rows
select * from approximate_row_count('table');
```

## Resources

- Docs: [`drop_chunks()`](https://docs.timescale.com/api/latest/hypertable/drop_chunks/#sample-usage)
- Talk: [Data retention in TimescaleDB](https://www.youtube.com/watch?v=FxWQS1rY7h8)

[^1]: ein `DELETE` markiert entpsrechende Rows zur Löschung ("dead tuple"); `VACUUM` trägt diese ungültig gewordenen Speicherbereiche in die Free Space Map (FSM) ein, sodass sie frei zur Wiederverwendung sind (vollständig leere Pages werden gelöscht)
