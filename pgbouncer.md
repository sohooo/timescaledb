# Talks

## Connection pooling routing and queueing with pgBouncer
- talk: [Connection pooling routing and queueing with pgBouncer](https://www.youtube.com/watch?v=x_XpPbfomso)
- queueing topic around min#8 relates to `telegraf's ---[pgbouncer]---> postgres` setup 
- protect database from overloading: set `pool_size`; only allow ~20 connections (maybe matching postgres' `max_connections`)
- pools are per user (connection entry in `[databases]`); => no useful pooling for lots of individual users

```ini
default_pool_size = 20
; reserve_pool_size = 5      # up to 5 more
; reserve_pool_timeout = 3   # kill connection
```

show some data; stuff for monitoring system:
- `maxwait`: longest wait of any client (num of seconds)
- `cl_waiting`: also interesting; but consider queueing

```sql
show pools;
-- cl_active: incoming clients
-- sv_active: outgoing connections to db server
-- maxwait:   <-- monitor!

-- pause/resume connections to mydb
pause mydb;
resume mydb;
```

online postgres restart, transparent for application:
- minor version upgrade
- restart for some parameter
- move instance somewhere else

```bash
psql -h pgbouncer -p 6432 -c 'pause mydb'
service postgresql restart
psql -h pgbouncer -p 6432 -c 'resume mydb'
```

also possible: online pgbouncer restart:
- starts new instance
- old instance transfers all state (open file descriptors, connections) to new instance

```bash
pgbouncer -R /path/to/pgbouncer.ini
# check systemd ExecReload!
```

admin:
> Additionally, the user name pgbouncer is allowed to log in without password, if the login comes via the Unix socket and the client has same Unix user UID as the running process. -- https://www.pgbouncer.org/usage.html



```mermaid
graph LR
    t1[telegraf #1]
    t2[telegraf #2]
    tn[telegraf #n]
    b{pgbouncer}
    p[(postgres)]

    t1 --> b
    t2 --> b
    tn --> b

    b ==> p
```

