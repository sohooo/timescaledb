# TimescaleDB Compression

- Platzersparnis, tw. > 90%
- sehr effizient für Aggregates (`first, last, min, max, sum, avg`)
- oft höhere Query Performance da weniger Disk Reads
- Update einzelner Compressed Rows ist teuer
- einzelne Spalten schnell selektierbar; `SELECT *` langsam

## Features

- ab v2.1: `ADD`/`RENAME` von Spalten in Compressed Chunks
- ab v2.3: `INSERT`s in Compressed Chunks möglich
- `JOIN`s mit Compressed Chunks möglich
- Continuous Aggregate basierend auf Compressed Chunks

## Notes

- bessere Compression bei höherem Chunk Size Interval; Switch auf `1 day`?

## Admin

```sql
-- alle Chunks einer Tabelle
select * from show_chunks('cpu');

-- einzelnen Compressed Chunk ansehen
-- 1000 Werte pro Row möglich; wie nahe dran?
select host, count(*) from _timescaledb_internal._hyper_49_1234_chunk
group by host;

-- effiziente Operation auf Compressed Hypertable
-- Bsp: Buffers: shared hit=7750 (Pages; jede Page hat 8kb)
EXPLAIN(buffers,ANALYZE)
SELECT avg(weight) from truck_log
WHERE time > now() - INTERVAL '2 weeks' AND truck_id < 10;
```

TimescaleDB Information Tables/Views:

```sql
-- Public API (non-underscore Schema)
-- num_chunks, compression=true?
select * from timescaledb_information.hypertables;
-- einzelne Chunks; range_start/end
select * from timescaledb_information.chunks where hypertable_name = 'cpu';
-- segmentby
select * from timescaledb_information.compression_settings where hypertable_name = 'cpu';

-- "Private" API
-- Compressed Hypertable finden: cpu ht_id 71; compressed_hypertable_id: 90
select * from _timescaledb_catalog.hypertable;
-- compression_algorithm_id
select * from _timescaledb_catalog.hypertable_compression where hypertable_id = 71;
select * from _timescaledb_catalog.compression_algorithm;
-- Compressed Data
-- Meta Infos: _ts_meta_count: num rows per segment
select * from _timescaledb_internal._compressed_hypertable_90 limit 10;
```

## Segmenting

- Query Planner effizienter, genau diese Rows aus Compressed Chunk zu filtern
- nicht sinnvoll bei Incrementing Integers
- Indexes von Main Hypertable werden nicht in Compressed HT übernommen
- in den meisten Fällen nur nach `time` Dimension segmentieren; zusätzliche Space Dimension (zB `host`) erst bei Distributed Hypertables wirklich performant
- nachfolgendes `ALTER` erzeugt Index auf `host` in Compressed Chunks

```sql
ALTER TABLE cpu SET (
  timescaledb.compress,
  timescaledb.compress_segmentby='host',
  timescaledb.compress_orderby='time'
);

-- Indexes auf Compressed Hypertable
select * from _timescaledb_catalog.hypertables; --> compressed_hypertable_id
select * from pg_indexes where table_name = '_compressed_hypertable_90';
-- Indexes erweiterbar
create index idx_compressed_segmentby_measurement
on _timescaledb_internal._compressed_hypertable_90 (sensor_id, measurement_type, _ts_meta_sequence_num);
```

## Helpers

```sql
CREATE OR REPLACE FUNCTION compression_stats(table_name regclass)
  RETURNS table(
    size_before_compression text,
    size_after_compression text,
    percent_difference numeric,
    ratio numeric
  ) AS
$$
DECLARE
BEGIN
  RETURN query
  WITH stats as (
    SELECT
      sum(before_compression_total_bytes) total_before,
      sum(after_compression_total_bytes) total_after
    FROM chunk_compression_stats(table_name)
    WHERE compression_status='Compressed'
  )
  SELECT pg_size_pretty(total_before), pg_size_pretty(total_after),
    round((total_before - total_after) / total_before::numeric * 100, 2)
    round(total_before / total_after::numeric, 1)
  FROM stats;
END
$$ LANGUAGE 'plpgsql';

select * from compression_stats('cpu');
```

## Resources

- [TimescaleDB compression 101](https://www.youtube.com/watch?v=RYPZH09MjwM)
- [TimescaleDB compression deep dive continued](https://www.youtube.com/watch?v=p3nggM9qpGo)
