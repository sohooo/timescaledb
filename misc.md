# Misc Notes

## SQL Stuff

via Talk [SQL Tricks of an Application DBA - Haki Benita](https://www.youtube.com/watch?v=PBfq14Jr2gU)

### Indexes, Statistics

Beantwortet die Frage, warum Postgres bei einer Query keinen Index verwendet (`activated=true`), bei einer anderen (`activated=false`) jedoch schon:

```sql
--> frequency = matching rows / total number of rows
select attname, n_distinct, most_common_vals, most_common_freqs
from pg_stats
where tablename = 'users' and attname = 'activated'; --> bool col; 990k/10k

--> daher besser: Partial Index
create index users_inactivated_part_idx on users(id) where not activated;
```

### Correlation

> Statistical correlation between physical row ordering and logical ordering of the column values

- `correlation=1`: auto incr. IDs
- `correlation~0`: wenig/keine Korrelation

```sql
--> created_at mit Random Timestamps, nicht aufsteigend!
select attname, correlation
from pg_stats
where tablename = 'sale' and attname in ('id', 'created_at'); --> id: 1, created_at: 0.01
```

#### Block Range Index (BRIN)

> BRIN is designed for handling very large tables in which certain columns have some natural correlation with their physical location on disk.

- nützlich für Cols mit hoher `correlation`
- typischerweise speichereffizient

### UNLOGGED Tables

- für Tabellen, die nur im Rahmen einer Transformation o.ä. erstellt werden (ETL, ...)
- dadurch kein WAL

```sql
create unlogged table staging_table (...);
```

### Invisible Indexes

- Würde ein Index etwas bringen?
- Wie wäre Execution Plan ohne diesen Index?

```sql
--> transactional DDL Feature von Postgres nutzen
begin;
drop index sale_created_at_idx;
explain select * from sale where created_at between '2022-05-01' and '2022-05-20';
rollback;
```

## EXLAIN ANALYZE

via Talk [A beginners guide to EXPLAIN ANALYZE – Michael Christofides](https://www.youtube.com/watch?v=31EmOKBP1PY)
